/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2016 Paweł Wegner
 *
 * Authors: Paweł Wegner<pawel.wegner95@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once

#include <ICloudProvider.h>

#include "filesystem/IDevice.h"

namespace medialibrary
{

namespace cp
{

class Device : public fs::IDevice
{
public:
    Device( cloudstorage::ICloudProvider::Pointer );

    const std::string& uuid() const override;
    bool isRemovable() const override;
    bool isPresent() const override;
    const std::string& mountpoint() const override;

    cloudstorage::ICloudProvider::Pointer provider() const;

private:
    cloudstorage::ICloudProvider::Pointer m_cloudprovider;
    std::string m_uuid;
    std::string m_mountpoint;
};
}
}
