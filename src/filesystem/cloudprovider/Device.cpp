/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2016 Paweł Wegner
 *
 * Authors: Paweł Wegner<pawel.wegner95@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "Device.h"

#include "logging/Logger.h"

namespace medialibrary
{

namespace cp
{

namespace
{
class AuthorizationCallback : public cloudstorage::ICloudProvider::ICallback
{
public:
    Status userConsentRequired( const cloudstorage::ICloudProvider& p ) override
    {
        LOG_INFO( "User consent required at " + p.authorizeLibraryUrl() );
        return Status::WaitForAuthorizationCode;
    }

    void accepted( const cloudstorage::ICloudProvider& ) override {}

    void declined( const cloudstorage::ICloudProvider& ) override {}

    void error( const cloudstorage::ICloudProvider&,
                const std::string& description ) override
    {
        LOG_ERROR( description );
    }
};
}

Device::Device( cloudstorage::ICloudProvider::Pointer provider )
    : m_cloudprovider( provider ),
      m_uuid( "cloudprovider-" + provider->name() ),
      m_mountpoint( provider->endpoint() )
{
    // TODO reuse refresh token
    // TODO provide ICryptoPP implementation
    // TODO provide IHttp implementation
    provider->initialize(
    {"", std::make_shared<AuthorizationCallback>(), nullptr, nullptr, {}} );
}

const std::string& Device::uuid() const
{
    return m_uuid;
}

bool Device::isRemovable() const
{
    return true;
}

bool Device::isPresent() const
{
    // TODO check if connected to the internet
    return true;
}

const std::string& Device::mountpoint() const
{
    return m_mountpoint;
}

cloudstorage::ICloudProvider::Pointer Device::provider() const
{
    return m_cloudprovider;
}
}
}
