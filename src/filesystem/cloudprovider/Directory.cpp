/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2016 Paweł Wegner
 *
 * Authors: Paweł Wegner<pawel.wegner95@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "Directory.h"

#include "filesystem/cloudprovider/File.h"

namespace medialibrary
{

namespace cp
{

Directory::Directory( std::shared_ptr<Device> device,
                      cloudstorage::IItem::Pointer item, const std::string& path )
    : m_device( device ), m_path( path ), m_item( item ) {}

const std::string& Directory::path() const
{
    return m_path;
}

const std::vector<std::shared_ptr<fs::IFile>>& Directory::files() const
{
    if ( m_files.size() == 0 && m_directories.size() == 0 ) read();
    return m_files;
}

const std::vector<std::shared_ptr<fs::IDirectory>>& Directory::dirs() const
{
    if ( m_files.size() == 0 && m_directories.size() == 0 ) read();
    return m_directories;
}

std::shared_ptr<fs::IDevice> Directory::device() const
{
    return m_device;
}

void Directory::read() const
{
    if ( !m_item )
        m_item = m_device->provider()->getItemAsync( path() )->result();
    auto t = m_device->provider()->listDirectoryAsync( m_item )->result();
    for ( auto item : t )
    {
        if ( item->type() == cloudstorage::IItem::FileType::Directory )
        {
            m_directories.emplace_back( std::make_shared<Directory>(
                                            m_device, item, path() + "/" + item->filename() ) );
        }
        else
        {
            auto i = m_device->provider()->getItemDataAsync( item->id() )->result();
            m_files.emplace_back(
                std::make_shared<File>( i->url(), path() + "/" + item->filename() ) );
        }
    }
}
}
}
