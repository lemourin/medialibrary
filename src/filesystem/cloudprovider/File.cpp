/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2016 Paweł Wegner
 *
 * Authors: Paweł Wegner<pawel.wegner95@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "File.h"

namespace medialibrary
{
namespace cp
{

File::File( const std::string& url, const std::string& path )
    : fs::CommonFile( path ), m_url( url ) {}

unsigned int File::lastModificationDate() const
{
    // TODO
    return 0;
}

const std::string& File::fullPath() const
{
    return m_url;
}
}
}
