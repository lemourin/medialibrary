/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2016 Paweł Wegner
 *
 * Authors: Paweł Wegner<pawel.wegner95@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include "CloudProvider.h"

#include <ICloudStorage.h>

#include "filesystem/cloudprovider/Device.h"
#include "filesystem/cloudprovider/Directory.h"
#include "logging/Logger.h"

namespace medialibrary
{

namespace factory
{

CloudProvider::CloudProvider( cloudstorage::ICloudProvider::Pointer provider )
    : m_device( std::make_shared<cp::Device>( provider ) )
{
}

std::shared_ptr<fs::IDirectory> CloudProvider::createDirectory(
    const std::string& path )
{
    std::lock_guard<std::mutex> lock( m_mutex );
    const auto it = m_directories.find( path );
    if ( it != end( m_directories ) ) return it->second;
    try
    {
        auto dir = std::make_shared<cp::Directory>( m_device, nullptr, path );
        m_directories[path] = dir;
        return dir;
    }
    catch ( const std::exception& ex )
    {
        LOG_ERROR( "Failed to create cp::IDirectory for ", path, ": ", ex.what() );
        return nullptr;
    }
}

std::shared_ptr<fs::IDevice> CloudProvider::createDevice( const std::string& )
{
    return m_device;
}

void CloudProvider::refresh()
{
    std::lock_guard<std::mutex> lock( m_mutex );
    m_directories.clear();
}
}
}
