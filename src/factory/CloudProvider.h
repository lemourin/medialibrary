/*****************************************************************************
 * Media Library
 *****************************************************************************
 * Copyright (C) 2016 Paweł Wegner
 *
 * Authors: Paweł Wegner<pawel.wegner95@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#pragma once

#include "factory/IFileSystem.h"
#include "filesystem/cloudprovider/Device.h"

#include <ICloudProvider.h>
#include <mutex>
#include <unordered_map>

namespace medialibrary
{

namespace factory
{

class CloudProvider : public IFileSystem
{
public:
    CloudProvider( cloudstorage::ICloudProvider::Pointer provider );

    std::shared_ptr<fs::IDirectory> createDirectory(
        const std::string& path ) override;

    std::shared_ptr<fs::IDevice> createDevice( const std::string& uuid ) override;

    void refresh() override;

private:
    std::mutex m_mutex;
    std::unordered_map<std::string, std::shared_ptr<fs::IDirectory>>
            m_directories;
    std::shared_ptr<cp::Device> m_device;
};
}
}
